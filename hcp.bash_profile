kendrickpath=${HOME}/HCP/kendrick

export PATH=$PATH:${kendrickpath}/ImageMagick-6.5.5/bin:${kendrickpath}/bin
export MAGICK_HOME=${kendrickpath}/ImageMagick-6.5.5
export DYLD_LIBRARY_PATH=${kendrickpath}/ImageMagick-6.5.5/lib

alias matlab="/Applications/MATLAB_R2010b.app/bin/matlab"

# added by Anaconda 2.0.1 installer
export PATH="${HOME}/anaconda/bin:$PATH"
