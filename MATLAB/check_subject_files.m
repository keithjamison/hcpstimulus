function [iscomplete stimtype_info] = check_subject_files(subjnum)
% Print a summary describing which files (saved .mat, eyetracking .edf/.asc files) 
%   are present for a given subject

stimtype_info = struct('name',{'FIX','MOV','RET'},'numscans',{4 4 6});

subjdir=sprintf('%s/%d',hcppath('data'),subjnum);
if(~exist(subjdir,'dir'))
    iscomplete = false;
    warning('Subject directory not found: %s',subjdir);
    return;
end

fprintf('\n%-8s %-8s %-10s %-10s %-10s\n','subj','type','.mat','eye.edf','eye.asc');

for s = 1:numel(stimtype_info)
    stimtype=stimtype_info(s).name;
    
    stimmat='';
    stimedf='';
    stimasc='';
    for i = 1:stimtype_info(s).numscans
        stimmatfiles = dir(sprintf('%s/%d_%s%d_7T_*.mat',subjdir,subjnum,stimtype,i));
        stimmat=[stimmat num2str(numel(stimmatfiles))];

        stimedffiles = dir(sprintf('%s/%d_%seyetrack%d_7T_*.edf',subjdir,subjnum,stimtype,i));
        stimedf=[stimedf num2str(numel(stimedffiles))];
        
        stimascfiles = dir(sprintf('%s/%d_%seyetrack%d_7T_*.asc',subjdir,subjnum,stimtype,i));
        stimasc=[stimasc num2str(numel(stimascfiles))];
    end
    stimtype_info(s).matcount=stimmat;
    stimtype_info(s).edfcount=stimedf;
    stimtype_info(s).asccount=stimasc;
    
    stimtype_info(s).complete = all([stimmat stimedf stimasc] == '1');
        
    fprintf('%-8d %-8s %-10s %-10s %-10s\n',subjnum,stimtype,stimmat,stimedf,stimasc);
end
fprintf('\n');

iscomplete = all([stimtype_info.complete]);