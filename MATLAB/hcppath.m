function p = hcppath(varargin)
% hcproot, matlab, movies, data, ptbroot, eyelink

if(nargin < 1)
    whichpath='hcproot';
else
    whichpath=varargin{1};
end

switch(lower(whichpath))
    case 'hcproot'
        testpaths={
            '/Users/7Tuser/HCP'
            '/Users/7tuser/Desktop/HCP'};
    case 'matlab'
        testpaths={
            '/Users/7Tuser/HCP/MATLAB'
            '/Users/7tuser/Desktop/HCP/MATLAB'};
    case 'movies'
        testpaths={
            '/Users/7Tuser/HCP/stimulus_movies'
            '/Users/7tuser/Desktop/HCP/stimulus_movies'};
    case 'data'
        testpaths={
            '/Users/7Tuser/HCP/Data'
            '/Users/7tuser/Desktop/HCP/Data'};
    case 'kendrick'
        testpaths={
            '/Users/7Tuser/HCP/kendrick'
            '/Users/7tuser/kendrick'
            '/Users/kjamison/Source/kendrick'};
    case 'ptbroot'
        testpaths={
            '/Applications/Psychtoolbox_64bit/Psychtoolbox'
            '/Applications/Psychtoolbox64/Psychtoolbox'
            '/Applications/Psychtoolbox'};
    case 'eyelink'
        testpaths={
            '/Applications/EyeLink/EDF_Access_API/Example/'};
end

p='';
for i = 1:numel(testpaths)
    if(exist(testpaths{i},'dir'))
        p=testpaths{i};
        break;
    end
end


if(isempty(p))
    warning('No path found for %s',whichpath);
end
