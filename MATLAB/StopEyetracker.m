function StopEyetracker(dataFileEL, dataFileEL_output, no_file)
if(~Eyelink('IsConnected'))
    return;
end

%after experiment ends
Eyelink('StopRecording'); %stop recording
Eyelink('CloseFile'); %close the datafile
    
if(~no_file)
    [pathToSaveFileIn, filenameEL, extEL] = fileparts(dataFileEL_output);
    dataFileELnew = [filenameEL extEL];
    

    status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
    fprintf('ReceiveFile status %d\n', status);
    %rename it something unique
    
    mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew ];
    system(mycmd);
    mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn dataFileELnew];
    system(mycmd);
end

Eyelink('Shutdown'); %terminate communication with eyetracker