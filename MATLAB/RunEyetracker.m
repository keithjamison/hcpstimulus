function [el dataFileEL] = RunEyetracker(win, screenRect)
dataFileEL = '';
el.targetbeep=0; % no beep
%open a link to the eyetracker, passing info about the PTB screen
el=EyelinkInitDefaults(win);

%check the connection to the eyelink. If it's not there, exit gracefully
if ~EyelinkInit
    fprintf('Eyelink Init aborted.\n');
    el = [];
    return;
end
%make sure the beeps are silenced?
% you need to pass the el structure back to the dispatch function
if ~isempty(el.callback)
    feval(el.callback,el);
end

% define beep sounds (frequency, volume, duration);
el.cal_target_beep=[0 0 0];
el.drift_correction_target_beep=[0 0 0];
el.targetbeep = false;
el.calibration_failed_beep = [0 0 0];
el.calibration_success_beep = [0 0 0];
el.drift_correction_failed_beep = [0 0 0];
el.drift_correction_success_beep = [0 0 0];
EyelinkUpdateDefaults(el);

%open a data file on the eyetracker host
%THE DATA FILENAME IS LIMITED TO 8.3 ON THE HOST !!!!!!!!!!!!!!
dataFileEL=[datestr(now,'mmddHHMM') '.edf'];
Eyelink('Openfile',dataFileEL);
%optional setup information to transmit
%users can overwrite during live calibration
Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, screenRect(3), screenRect(4));
Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, screenRect(3), screenRect(4));
Eyelink('command', 'calibration_type = HV5'); % set calibration type.

disp('Waiting for eyetracker calibration...');
EyelinkDoTrackerSetup(el); %run calibration

Eyelink('StartRecording'); %start recording
% record a few samples before we actually start displaying
WaitSecs(0.2);