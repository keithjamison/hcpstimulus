function hcp_clear()

if(Eyelink('IsConnected'))
    Eyelink('StopRecording');
    Eyelink('CloseFile');
    Eyelink('Shutdown');
end
Screen('CloseAll');
