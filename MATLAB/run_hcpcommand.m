
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function run_hcpcommand(stimtype,subjnum,runnum,expire_sec)
% run_hcpcommand(stimtype,subjnum,runnum,expire_sec)


global did_forcequit;
if(did_forcequit)
    error('Operator forcequit previous stimulus. Not automatically starting next one');
    return;
end
did_forcequit=false;

%%%%%%%%%%%
fprintf('\n%s(''%s'',%d,%d,%d)\n\n',mfilename,stimtype,subjnum,runnum,expire_sec);
%%%%%%%%%%%%

cmdfmt = 'hcpobj=struct(''subjnum'',%d,''stimtype'',''%s'',''runnum'',%d,''expiration'',''%s'',''dateformat'',''%s''); %s';   
datefmt = 'yyyymmddHHMMSSFFF';

stimscript = '';
switch stimtype
    case 'fix'
        stimscript = 'AJCfix';
    case 'mov'
        stimscript = 'AJCmov';
    case 'ret'
        stimscript = 'AJCret';
    otherwise
end

if(~isempty(stimscript))
    cmd_expire = datestr(now+expire_sec/(60*60*24),datefmt);
    cmdstr = sprintf(cmdfmt,subjnum,stimtype,runnum,cmd_expire,datefmt,stimscript);
    %fprintf('%s\n',cmdstr);
    evalin('base',cmdstr);
end
