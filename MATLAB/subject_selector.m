function newsubj = subject_selector()

newsubj=[];

subjroot=hcppath('data');

D = dir(subjroot);
D = D([D.isdir]);
D = D(~cellfun(@isempty,regexp({D.name}, '^[0-9]{2,}+$')));

%find earliest file date in each directory
% (avoids problem where moving files resets directory mod date)
for i = 1:numel(D)
    Ds = dir([subjroot '/' D(i).name]);
    d = min([Ds.datenum]);
    D(i).datenum = d;
end

[~,sidx] = sort([D.datenum],'descend');
D = D(sidx);
subjlist = arrayfun(@(d)(sprintf('%-10s\t%-12s ',d.name, datestr(d.datenum,'yyyy-mm-dd'))),D,'uniformoutput',false);

%add extra "_" to visually separate weekends
dd = diff([D.datenum]);
subjlist(abs(dd) > 3) = regexprep(subjlist(abs(dd) > 3),' $','_');

[s,v] = listdlg('PromptString','Select existing subject:',...
    'SelectionMode','single',...
    'ListString',subjlist);


if(v > 0 && s > 0)
    newsubj = str2num(D(s).name); %#ok<ST2NM>
end
