if ~exist('kspr','var')
    run kendrickstartup
end

cd ~/Documents/MATLAB
hcp_clear;
clear functions


is_hcpgui = false;
if(exist('hcpobj','var'))
    d = datenum(hcpobj.expiration,hcpobj.dateformat);
    if(now <= d && strcmp(hcpobj.stimtype,'fix'))
        subjnum = hcpobj.subjnum;
        runNum = hcpobj.runnum;
        clear hcpobj;
        is_hcpgui = true;
    else
        %command expired... do not run
        clear hcpobj;
        return;
    end
end


if(~is_hcpgui)
    % 20140601AJC
    if exist('subjnum','var') %&& isempty(subjnum)
        sprintf('Is subj id: %d (y/n)? ', subjnum),
        answeryn = input('> ', 's');
        if answeryn == 'n'
            clear('subjnum');
        end
    end
    if ~exist('subjnum','var')
        disp('(subj id = 0 for no recording)');
        subjnum = input('What is the subj id? ');
    end

    disp('|-------------------------------------------')
    disp('|    1 = MOV1 fixation cross (Session 1)')
    disp('|    2 = RET fixation cross (Session 2)')
    disp('|    3 = DIFF fixation cross (Session 3)')
    disp('|    4 = MOV2 fixation cross (Session 4)')
    disp('|--------------------------------------------')
    runNum = input('Which session number to run? > ');
end

filename = sprintf('%d_FIX%d_7T_%s', subjnum, runNum, gettimestring);

pathToSaveFileIn=sprintf('/Users/7Tuser/Desktop/HCP/Data/%d/',subjnum);
if(~exist(pathToSaveFileIn,'dir'))
    mkdir(pathToSaveFileIn);
end

movieduration = 30;
subjshow = 'Loading fixation cross... Please try to stay awake!';

switch runNum
    case 1
    case 2
    case 3
    case 4
    otherwise
        error('unknown runNum')
end

%% Schedule start immediately 
eyetracking = 0; i=0;
% Switch KbName into unified mode: It will use the names of the OS-X
% platform on all platforms in order to make this script portable:
KbName('UnifyKeyNames');
ttlPulse = KbName('5%'); % 20140120AV
space=KbName('SPACE');
esc=KbName('ESCAPE');
ttlStamps = zeros(1,16*60); % store clock stamps 20140430AV
deviceIndex = [];

try
    AssertOpenGL;
    %background=[128, 128, 128]; % original grey
    background=[255, 255, 255]; %white
    
    % Open onscreen window:
    oldLevel = Screen('Preference', 'Verbosity', 2); %quiet! default=3
    screen=max(Screen('Screens'));
    [win,screenRect] = Screen('OpenWindow', screen, 0);
    
    el = [];
    dataFileEL = '';
    % Eyetracker prep
    switch runNum
        case {1 2 3 4 13}    
            [el, dataFileEL] = RunEyetracker_KJ(win,screenRect);
            if(isempty(el))
                Screen('CloseAll');
                break;
            end
            eyetracking = 1;
    end

    Screen('FillRect', win, background); % Clear screen to background color
    Screen('Flip',win);  % Initial display and sync to timestamp
    blocking = 1; % Use blocking wait for new frames by default
    
    ttlCnt = 0; % counting number of TTLs
        
    % Show title while movie is loading/prerolling: 
    oldtextsize = Screen('TextSize', win, 60);
    %DrawFormattedText(win, tstring, sx, sy, color, wrapat, 
    %                   flipHorizontal, flipVertical, vSpacing, righttoleft)
    DrawFormattedText(win, subjshow, 'center', 'center', [0 0 0], 30, [], [], 1.5);
    Screen('Flip', win);

    % Wait for TTL before starting movie (20140104AV)
    keyIsDown=false;
    did_escape=false;
    disp('Waiting for TTL pulse...')
    while ~keyIsDown
        [keyIsDown,secs,keyCode]=KbCheck(-3);
        
        if (keyIsDown==1 && keyCode(esc))
            did_escape = true;
            break;
        end;
        if ~(keyIsDown && keyCode(ttlPulse))
            keyIsDown = 0;
        end
    end
    
    if(did_escape)
        fprintf('\nEscape key detected.  Exiting prematurely.\n');
        hcp_clear;
        return;
    end
    
    Screen('FillRect', win, [0 0 0]); %back to black
    % Play 'movie', at a playbackrate = 1, with endless loop=0 and 1.0 == 100% audio volume.
    %Screen('PlayMovie', movie, rate, loop, audio);

    tex = Screen('MakeTexture',win,zeros(screenRect(4),screenRect(3)));
    Screen('TextSize', tex, 60);
    Screen('FillRect',tex,[255 255 255]);
    DrawFormattedText(tex, '+', 'center', 'center', [0 0 0], 30, [], [], 1.5);
    
    %%%%%%%%%%%%%%%%%%%
    % 20140802KJ - identify the button box input device for KbQueue
    [kbidx,~,kbdev] = GetKeyboardIndices();
    if(~isempty(kbidx))
        is_curdes = cellfun(@(x)(strcmp(x.product,'932') ...
            && strcmp(x.manufacturer,'Current Designs, Inc.')),kbdev);
        deviceIndex = kbidx(find(is_curdes,1,'first'));
        
        is_curdes = cellfun(@(x)(strcmp(x.product,'Apple Keyboard') ...
            && strcmp(x.manufacturer,'Apple, Inc')),kbdev);
        deviceIndex = kbidx(find(is_curdes,1,'first'));
    end
    %%%%%%%%%%%%%%%%%%%
    
    % 20140501AV
    KbQueueCreate(deviceIndex);
    while KbCheck(-3); end % Wait until all keys are released.
    KbQueueStart(deviceIndex);

    disp('TTL pulse detected, starting fixation ...')
    timeStart = GetSecs;
    ttlCnt = ttlCnt+1;
    ttlStamps(ttlCnt) = timeStart;
    if eyetracking
          Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
    end

    % Infinite playback loop: Fetch video frames and display them...
    while 1
        
        [ keyIsDown, keyCode]=KbQueueCheck(deviceIndex); % 20140501AV  
        secs = keyCode(logical(keyCode));
        %[keyIsDown,secs,keyCode]=KbCheck(-3); %#ok<ASGLU>
        
        % log time stamps for ttl pulses 20140430AV
        if (keyIsDown==1 && keyCode(ttlPulse))
            ttlCnt = ttlCnt+1;
            ttlStamps(ttlCnt) = secs;
            if eyetracking
                Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
            end
        end;
        
        % 20140802KJ need to use separate KbCheck to listen for escapes
        %   from keyboards (KbQueue only listens to button box)
        [kb_keyIsDown,kb_secs,kb_keyCode]=KbCheck(-3); %#ok<ASGLU>

        if (kb_keyIsDown==1 && kb_keyCode(esc))
            break;
        end;

        if(GetSecs-timeStart > movieduration)
            break;
        end

        Screen('DrawTexture', win, tex, [], [], [], [], [], [], []);
        Screen('Flip', win); % Update display
        i=i+1;

    end
    
    timeEnd = GetSecs;
    duration = timeEnd - timeStart;
    fprintf('Elapsed time %f seconds, for %i frames.\n', duration, i);

    Screen('Flip', win);
    KbReleaseWait(-3);
    
    Screen('Close', tex); % Release texture

        
    % 20140501AV
    KbEventFlush(deviceIndex);
    KbQueueRelease(deviceIndex);
        
    % Eyetracker cleanup
      if eyetracking     
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile'); %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_FIXeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        Eyelink('Shutdown'); %terminate communication with eyetracker
        mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
      end
    Screen('CloseAll');
    
catch err%#ok<*CTCH>
    err
    % Error handling: Close all windows and movies, release all resources.
    if eyetracking        
        %StopEyetracker(dataFileEL, dataFileELnew, pathToSaveFileIn,1);
        
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile');  %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_FIXeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        Eyelink('Shutdown');  %terminate communication with eyetracker
        mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);    
    end
    Screen('CloseAll');
    rethrow(err)
end

if subjnum
    save(fullfile(pathToSaveFileIn,filename),'duration','ttlCnt','ttlStamps','timeStart','timeEnd')
end

FlushEvents
