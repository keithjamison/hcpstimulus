if ~exist('kspr','var')
    run kendrickstartup
end

cd(hcppath('matlab'));
%hcp_clear;
%clear functions;

% did this order make it not crash?
%clear functions;
clear PsychHID;
Screen('CloseAll'); 

is_hcpgui = false;
if(exist('hcpobj','var'))
    d = datenum(hcpobj.expiration,hcpobj.dateformat);
    if(now <= d && strcmp(hcpobj.stimtype,'mov'))
        subjnum = hcpobj.subjnum;
        runNum = hcpobj.runnum;
        clear hcpobj;
        is_hcpgui = true;
    else
        %command expired... do not run
        clear hcpobj;
        return;
    end
end

if(~is_hcpgui)
    % 20140601AJC
    if exist('subjnum','var') %&& isempty(subjnum)
        sprintf('Is subj id: %d (y/n)? ', subjnum),
        answeryn = input('> ', 's');
        if answeryn == 'n'
            clear('subjnum');
        end
    end
    if ~exist('subjnum','var')
        disp('(subj id = 0 for no recording)');
        subjnum = input('What is the subj id? ');
    end

    disp('|-------------------------------------------')
    disp('|    0 = test + audio adjust (~3.5 min)')
    disp('|    11 = test + question1 (~21 sec)')
    disp('|    12 = test + question2 (~17 sec)')
    disp('|');
    disp('|    1 = movie run 1 (session MOV1)')
    disp('|    2 = movie run 2 (session MOV1)')
    disp('|    3 = movie run 3 (session MOV2)')
    disp('|    4 = movie run 4 (session MOV2)')
    disp('|--------------------------------------------')
    runNum = input('Which run number to run? > ');
end

filename = sprintf('%d_MOV%d_7T_%s', subjnum, runNum, gettimestring);

pathToSaveFileIn=sprintf('%s/%d/',hcppath('data'),subjnum);
if(~exist(pathToSaveFileIn,'dir'))
    mkdir(pathToSaveFileIn);
end

audioeq='eq205'; %thick buds
%audioeq='eq325'; %slim buds

movieanswer = '';

switch runNum
    case 0
        moviename = ['7T_MN_test_' audioeq '.mp4'];
        %subjshow = 'Using the controller, adjust volume until voices are loud are clear: press 1 to decrease volume, press 4 to increase volume';
        subjshow = 'Please listen for the dialogue in this movie clip and let the operator know if you cannot hear what is being said';
    case 11
        moviename = ['audio_test_question1_' audioeq '.mp4'];
        subjshow = 'How long ago did he and his family come down to the Bahamas?';
        movieanswer = '10 years';
    case 12
        moviename = ['audio_test_question2_' audioeq '.mp4']
        subjshow = 'The guide has the ability to find what?';
        movieanswer = 'A rhythm [between the tides, the clouds, what the fish are doing, and to get us in the right spot]';
    case 1
        moviename = ['7T_MOVIE1_CC1_' audioeq '_scaled_v2.mp4'];
        %moviename = '7T_MOVIE1_CC1_v2.mp4';
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 2 
        moviename = ['7T_MOVIE2_HO1_' audioeq '_scaled_v2.mp4'];
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 3
        moviename = ['7T_MOVIE3_CC2_' audioeq '_scaled_v2.mp4'];
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 4
        moviename = ['7T_MOVIE4_HO2_' audioeq '_scaled_v2.mp4'];
        subjshow = 'Loading movie clips... Please try to stay awake!';
    otherwise
        error('unknown runNum')
end

%% Schedule start immediately 
eyetracking = 0; i=0;
% Switch KbName into unified mode: It will use the names of the OS-X
% platform on all platforms in order to make this script portable:
KbName('UnifyKeyNames');
ttlPulse = KbName('5%'); % 20140120AV
space=KbName('SPACE');
esc=KbName('ESCAPE');
ttlStamps = zeros(1,16*60); % store clock stamps 20140430AV
ttlFrames = zeros(1,16*60); % store the frame numbers as well 20150123KJ

ttlStart = [];
timeMovieEnd = [];

deviceIndex = [];

try
    AssertOpenGL;
    %background=[128, 128, 128]; % original grey
    %background=[255, 255, 255]; %white
    background=[250, 250, 250]; %just below white to avoid projector problem
    
    % Open onscreen window:
    oldLevel = Screen('Preference', 'Verbosity', 2); %quiet! default=3
    screen=max(Screen('Screens'));
    [win,screenRect] = Screen('OpenWindow', screen, 0);

    
    el = [];
    dataFileEL = '';
    % Eyetracker prep
    switch runNum
        case {1 2 3 4}
            %if subjnum == 9999
                [el, dataFileEL] = RunEyetracker(win,screenRect);
                if(isempty(el))
                    Screen('CloseAll');
                    break;
                end
                eyetracking = 1;
            %end
    end
    
    Screen('FillRect', win, background); % Clear screen to background color
    Screen('Flip',win);  % Initial display and sync to timestamp
    blocking = 1; % Use blocking wait for new frames by default
    
    ttlCnt = 0; % counting number of TTLs
        
    % Show title while movie is loading/prerolling: 
    oldtextsize = Screen('TextSize', win, 60);
    %DrawFormattedText(win, tstring, sx, sy, color, wrapat, 
    %                   flipHorizontal, flipVertical, vSpacing, righttoleft)
    DrawFormattedText(win, subjshow, 'center', 'center', [0 0 0], 30, [], [], 1.5);
    Screen('Flip', win);
    fullmoviename = [ hcppath('movies') filesep moviename ];
    
    % Open movie file and retrieve basic info about movie:
    [movie movieduration fps imgw imgh] = Screen('OpenMovie', win, fullmoviename, [], []);

    fprintf('Movie: %s  : %f seconds duration, %f fps, w x h = %i x %i...\n',...
        fullmoviename, movieduration, fps, imgw, imgh);

    % Wait for TTL before starting movie (20140104AV)
    keyIsDown=false;
    did_escape = false;
    disp('Waiting for TTL pulse...')
    
    while ~keyIsDown
        [keyIsDown,secs,keyCode]=KbCheck(-3);
        
        if (keyIsDown==1 && keyCode(esc))
            did_escape = true;
            break;
        end;
        
        if ~(keyIsDown && keyCode(ttlPulse))
            keyIsDown = 0;
        end
                

        
    end
    
    ttlStart = secs;
    
    if(did_escape)
        fprintf('\nEscape key detected.  Exiting prematurely.\n');
        hcp_clear;
        return;
    end
    
    Screen('FillRect', win, [0 0 0]); %back to black
    % Play 'movie', at a playbackrate = 1, with endless loop=0 and 1.0 == 100% audio volume.
    %Screen('PlayMovie', movie, rate, loop, audio);
    Screen('PlayMovie', movie, 1, 0, 1.0);


    %%%%%%%%%%%%%%%%%%%
    % 20140802KJ - identify the button box input device for KbQueue
    [kbidx,~,kbdev] = GetKeyboardIndices();
    if(~isempty(kbidx))
        is_curdes = cellfun(@(x)(strcmp(x.product,'932') ...
            && strcmp(x.manufacturer,'Current Designs, Inc.')),kbdev);
        deviceIndex = kbidx(find(is_curdes,1,'first'));
    end
    %%%%%%%%%%%%%%%%%%%
    
    
    % 20140501AV
    KbQueueCreate(deviceIndex);
    while KbCheck(-3); end % Wait until all keys are released.
    KbQueueStart(deviceIndex);

    disp('TTL pulse detected, starting movie...')
    timeStart = GetSecs;
    ttlCnt = ttlCnt+1;
    ttlStamps(ttlCnt) = timeStart;
    ttlFrames(ttlCnt) = i;
    if eyetracking
          Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
    end

    % Infinite playback loop: Fetch video frames and display them...
    while 1
        
        [ keyIsDown, keyCode]=KbQueueCheck(deviceIndex); % 20140501AV  
        secs = keyCode(logical(keyCode));
        %[keyIsDown,secs,keyCode]=KbCheck(-3); %#ok<ASGLU>

        % log time stamps for ttl pulses 20140430AV
        if (keyIsDown==1 && keyCode(ttlPulse))
            ttlCnt = ttlCnt+1;
            ttlStamps(ttlCnt) = secs;
            ttlFrames(ttlCnt) = i;
            if eyetracking
                Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
            end
        end;
        
        % 20140802KJ need to use separate KbCheck to listen for escapes
        %   from keyboards (KbQueue only listens to button box)
        [kb_keyIsDown,kb_secs,kb_keyCode]=KbCheck(-3);
        if (kb_keyIsDown==1 && kb_keyCode(esc))
            break;
        end;
        
        % Only perform video image fetch/drawing if playback is active
        % and the movie actually has a video track (imgw and imgh > 0):
        if ((imgw>0) && (imgh>0))
            % Return next frame in movie, in sync
            % tex is either the positive texture handle or zero if no
            tex = Screen('GetMovieImage', win, movie, blocking);

            % Valid texture returned?
            if tex < 0 % No? Abort playback loop:
                timeMovieEnd = GetSecs;
                disp('invalid texture returned)')
                break;
            end

            if tex == 0 % Just sleep a bit and then retry.
                WaitSecs('YieldSecs', 0.005);
                continue;
            end

            % Draw the new texture immediately to screen:
            Screen('DrawTexture', win, tex, [], [], [], [], [], [], []);
            Screen('Flip', win); % Update display
            Screen('Close', tex); % Release texture
            
            i=i+1; % Framecounter
        end
    end
    
    timeEnd = GetSecs;
    duration = timeEnd - timeStart;
    fprintf('Elapsed time %f seconds, for %i frames.\n', duration, i);

    Screen('Flip', win);
    KbReleaseWait(-3);
    
    Screen('PlayMovie', movie, 0); % Done. Stop playback
    Screen('CloseMovie', movie); % Close movie object
    
    % 20140501AV
    KbEventFlush(deviceIndex);
    KbQueueRelease(deviceIndex);
        
    % Eyetracker cleanup
      if eyetracking     
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile'); %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_MOVeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        
        fprintf('Shutting down eyetracker...\n');
        Eyelink('Shutdown'); %terminate communication with eyetracker
        fprintf('\n\nConverting .edf to ASCII....\n');
        mycmd=[hcppath('eyelink') '/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
      end
    Screen('CloseAll');
    
catch err%#ok<*CTCH>
    err
    
    % Error handling: Close all windows and movies, release all resources.
    if eyetracking        
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile');  %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_MOVeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        Eyelink('Shutdown');  %terminate communication with eyetracker
        mycmd=[hcppath('eyelink') '/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);    
    end
    Screen('CloseAll');
    rethrow(err);
end

if(~isempty(movieanswer))
    fprintf('\n\nQuestion: %s\nCorrect Answer: %s\n\n', subjshow, movieanswer);
end

if subjnum
    save(fullfile(pathToSaveFileIn,filename),'duration','ttlCnt','ttlStamps','timeStart','timeEnd','ttlFrames','timeMovieEnd','ttlStart');
end

FlushEvents
