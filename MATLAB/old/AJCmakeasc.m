thepath=hcppath('data');

cd(thepath)
edflist = dir('*.edf');

for i = 1:length(edflist) 
    fprintf('i = %d\n', i);
    [pathstr, name, ext] = fileparts(edflist(i).name);
    ascname = [name '.asc'];
    if ~exist(ascname, 'file')
        mycmd=[hcppath('eyelink') '/edf2asc ' thepath name];
        system(mycmd);
    else
        disp('File Exists: '),
        disp(ascname)
    end
end