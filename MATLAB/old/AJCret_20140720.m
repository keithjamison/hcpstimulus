if ~exist('kspr','var')
    run kendrickstartup
end

cd ~/Documents/MATLAB
sca

is_hcpgui = false;
if(exist('hcpobj','var'))
    d = datenum(hcpobj.expiration,hcpobj.dateformat);
    if(now <= d && strcmp(hcpobj.stimtype,'ret'))
        subjnum = hcpobj.subjnum;
        trialnum = hcpobj.runnum;
        clear hcpobj;
        is_hcpgui = true;
    else
        %command expired... do not run
        clear hcpobj;
        return;
    end
end

if(~is_hcpgui)
    % ask the user what to run
    if exist('subjnum','var') %&& isempty(subjnum)
        sprintf('Is subj id: %d (y/n)? ', subjnum),
        answeryn = input('> ', 's');
        if answeryn == 'n'
            clear('subjnum');
        end
    end
    if ~exist('subjnum','var')
        disp('(subj id = 0 for no recording)');
        subjnum = input('What is the subj id? ');
    end

    trialnum = input('What run number (1=CCW, 2=CW, 3=expand, 4=contract, 5=multibar, 6=multibar2)? ');
end

pathToSaveFileIn=sprintf('/Users/7Tuser/Desktop/HCP/Data/%d/',subjnum);
if(~exist(pathToSaveFileIn,'dir'))
    mkdir(pathToSaveFileIn);
end

switch trialnum
    case 1
        setnum = 89; runnum = 1;
    case 2
        setnum = 90; runnum = 1;
    case 3
        setnum = 91; runnum = 1;
    case 4
        setnum = 92; runnum = 1;
    case 5
        setnum = 93; runnum = 1;
    case 6
        setnum = 93; runnum = 2;
    otherwise
        print "not valid run number"
        quit
end
% setup
offset = [];
movieflip = [0 0];
frameduration = 4;
  %fixationinfo = {uint8(repmat([255; 255-lumstep; 255-2*lumstep],[1 3])) 1};  % dot colors and alpha value
fixationinfo = {uint8([255 0 0; 0 0 0; 255 255 255])    0.5};
fixationsize =   10;%6;  % this controls the dot size
tfun = @() fprintf('STIMULUS STARTED.\n');
ptonparams = {[1024 768 60 32],[],0};
%ptonparams = {[],[],0};
meanchange = 3;        % dot changes occur with this average interval (in seconds)
changeplusminus = 2;   % plus or minus this amount (in seconds)
grayval = uint8(127);
triggerkey = '5';
trialparams = [];
gridalpha = 0.2;  % alpha level (fraction) for fixation grid

% some prep
if ~exist('images','var')
  images = [];
  maskimages = [];
end

filename = sprintf('%d_RET%d_7T_%s', subjnum, trialnum, gettimestring); %AJC 060114
eyelinkfile = sprintf('%d_RETeyetrack%d_7T_%s.edf', subjnum, trialnum, gettimestring); 

soafun = @() round(meanchange*(60/frameduration) + changeplusminus*(2*(rand-.5))*(60/frameduration));

% deal with fixation grid
ringloc = [0.0459039400750229 0.111549700360089 0.241768568669444 0.497791004797104 1];
specialoverlay = repmat(uint8(255*(1-drawpolargrid(768+10,4,768/2,{ringloc},1,[0 0 0],-1))),[1 1 4]);
specialoverlay = specialoverlay(6:end-5,6:end-5,:);
specialoverlay(:,:,1:3) = uint8(255);
specialoverlay(:,:,4) = specialoverlay(:,:,4) * gridalpha;

% run experiment
stimfile = '~/kendrick/stimuli.final/workspaces/workspace_retinotopyCaltsmash.mat';
stimfileextra = '~/kendrick/stimuli.final/workspaces/workspace_convert10.mat';

%%%%%%%%%%%%% input

iscolor = 1;
numrep = 1;
con = 100;

%%%%%%%%%%%%% load in the stimuli

if ~exist('images','var') || isempty(images)

  % load images
  load(stimfile,'images','maskimages');
  if ~exist('maskimages','var')
    maskimages = {};
  end
  if setnum(1)==6  % in this case, we have to use the newer versions of certain classes
    images0 = loadmulti(stimfileextra,'images');
    wh = cellfun(@(x) ~isempty(x),images0);
    images(wh) = images0(wh);
    clear images0;
  end
  
end
numinclass = cellfun(@(x) size(x,choose(iscolor,4,3)),images);  % a vector with number of images in each class

%%%%%%%%%%%%% perform run-specific randomizations (NOTE THAT THERE ARE HARD-CODED CONSTANTS IN HERE)

% some abbrevations for the retinotopy cases
totalframesstandard = 300*15;  % 300 s exactly
cycleslots = 22*15 + (1:8*32*15);  % after 22-s rest, 8 cycles of 32 s
mashcycles = randintrange(1,100,[1 8*32*15],1);  % long string of 8*32 stimuli
mashgapcycles = [];  % interrupted 28/4 stimuli
for nn=1:8
mashgapcycles = [mashgapcycles randintrange(1,100,[1 28*15],1) zeros(1,4*15)];
end
mashgapcyclesSP = mashgapcycles;  % special for contracting-ring case
mashgapcyclesSP(1:32*15:end) = 0;
mashgaponecyclefun = @() randintrange(1,100,[1 28*15],1);  % just do stimuli for one 28-s thing
standardcycles = repmat(1:32*15,[1 8]);        % repeat a regular 32-s stim 8 times
reversecycles = repmat([1 32*15:-1:2],[1 8]);  % repeat a reversed 32-s stim 8 times
standardgapcycles = repmat([1:28*15 zeros(1,4*15)],[1 8]);       % regular order for 28/4 interruptions
reversegapcycles = repmat([0 28*15:-1:2 zeros(1,4*15)],[1 8]);   % reversed order for contracting-ring-specific!
standardgaponecycle = 1:28*15;
reversegaponecycle = [1 28*15:-1:2];  % note the first 1 is really a blank anyway.

% init of framecolor (might be overridden later)
framecolor = [];

% figure out frameorder (handle the special retinotopy cases first)
switch setnum(1)

case 89

  % init
  frameorder = zeros(2,totalframesstandard);
  
  % handle all cycles of stimulus
  maskoffset = 0;
  frameorder(1,cycleslots) = mashcycles;
  frameorder(2,cycleslots) = maskoffset + standardcycles;

case 90

  % init
  frameorder = zeros(2,totalframesstandard);
  
  % handle all cycles of stimulus
  maskoffset = 0;
  frameorder(1,cycleslots) = mashcycles;
  frameorder(2,cycleslots) = maskoffset + reversecycles;

case 91

  % init
  frameorder = zeros(2,totalframesstandard);
  
  % handle all cycles of stimulus
  maskoffset = 32*15;
  frameorder(1,cycleslots) = mashgapcycles;
  frameorder(2,cycleslots) = copymatrix(maskoffset + standardgapcycles,standardgapcycles==0,0);

case 92

  % init
  frameorder = zeros(2,totalframesstandard);
  
  % handle all cycles of stimulus
  maskoffset = 32*15;
  frameorder(1,cycleslots) = mashgapcyclesSP;
  frameorder(2,cycleslots) = copymatrix(maskoffset + reversegapcycles,reversegapcycles==0,0);

case 93

  % init
  frameorder = zeros(2,totalframesstandard);
  
  % L to R
  maskoffset = 32*15+28*15+0*(28*15);
  slot0 = 16*15 + 0*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + standardgaponecycle;

  % D to U
  maskoffset = 32*15+28*15+2*(28*15);
  slot0 = 16*15 + 1*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + standardgaponecycle;

  % R to L
  maskoffset = 32*15+28*15+0*(28*15);
  slot0 = 16*15 + 2*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + reversegaponecycle;

  % U to D
  maskoffset = 32*15+28*15+2*(28*15);
  slot0 = 16*15 + 3*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + reversegaponecycle;

  % LL to UR
  maskoffset = 32*15+28*15+1*(28*15);
  slot0 = 16*15 + 4*(32*15) + 12*15 + 0*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + standardgaponecycle;

  % LR to UL
  maskoffset = 32*15+28*15+3*(28*15);
  slot0 = 16*15 + 4*(32*15) + 12*15 + 1*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + standardgaponecycle;

  % UR to LL
  maskoffset = 32*15+28*15+1*(28*15);
  slot0 = 16*15 + 4*(32*15) + 12*15 + 2*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + reversegaponecycle;

  % UL to LR
  maskoffset = 32*15+28*15+3*(28*15);
  slot0 = 16*15 + 4*(32*15) + 12*15 + 3*(32*15) + (1:28*15);
  frameorder(1,slot0) = mashgaponecyclefun();
  frameorder(2,slot0) = maskoffset + reversegaponecycle;

end
  clear framedesign0 framecolordesign0;

  % ah, repeat it
frameorder = repmat(frameorder,[1 numrep]);
if exist('framecolordesign','var')
  framecolor = repmat(framecolor,[1 numrep]);
end
% if exist('stimclassrec','var')
%   stimclassrec = repmat(stimclassrec,[1 numrep]);
% end

if iscell(fixationinfo{1})
    fixationorder = fixationinfo{1};
    fixationcolor = [];

  else
    fixationorder = zeros(1,1+size(frameorder,2)+1);
    lastfix = 0;  % act as if a fixation flip is shown just before we start the movie, which we interpret as starting at 1
    while 1
      lastfix = lastfix + feval(soafun);  % figure out next fixation flip
      if lastfix <= size(frameorder,2)  % if we're still within the duration of the movie
        fixationorder(1+lastfix) = 1;  % be careful; the 1+ is necessary because there is an initial entry indicating what happens before the movie even starts
      else
        break;
      end
    end
    % deal with case of alpha flipping
    if length(fixationinfo)==3
      isreg = fixationorder==0;
      isflip = fixationorder==1;
      fixationorder(isreg) = fixationinfo{2};
      fixationorder(isflip) = fixationinfo{3};
      fixationcolor = fixationinfo{1};
    % deal with case of color changes
    else
      cur = 1;  % start with the first fixation dot color
      for q=1:length(fixationorder)
        if fixationorder(q)==1
          cur = firstel(permutedim(setdiff(1:size(fixationinfo{1},1),cur)));  % change to a new color
        end
        fixationorder(q) = -cur;
      end
      fixationorder = [fixationorder fixationinfo{2}];  % tack on alpha value
      fixationcolor = fixationinfo{1};
    end

end

%%%%%%%%%%%%% show the movie

% setup PT
oldclut = pton(ptonparams{:});

% initialize, setup, calibrate, and start eyelink
if ~isempty(eyelinkfile)
  el.targetbeep=0; % no beep
  eyelinkfile_unique = eyelinkfile;  
  eyelinkfile =[datestr(now,'yymmdd') '.edf'];

  assert(EyelinkInit()==1);
  win = firstel(Screen('Windows'));

  
  el = EyelinkInitDefaults(win);
  if ~isempty(el.callback)
    PsychEyelinkDispatchCallback(el);
  end

  % define beep sounds (frequency, volume, duration);
  el.cal_target_beep=[0 0 0];
  el.drift_correction_target_beep=[0 0 0];
  el.targetbeep = false;
  el.calibration_failed_beep = [0 0 0];
  el.calibration_success_beep = [0 0 0];
  el.drift_correction_failed_beep = [0 0 0];
  el.drift_correction_success_beep = [0 0 0];
  EyelinkUpdateDefaults(el);
  [wwidth,wheight] = Screen('WindowSize',win);  % returns in pixels
  fprintf('Pixel size of window is width: %d, height: %d.\n',wwidth,wheight);
  Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld',0,0,wwidth-1,wheight-1);
  Eyelink('message','DISPLAY_COORDS %ld %ld %ld %ld',0,0,wwidth-1,wheight-1);
  Eyelink('command','calibration_type = HV5');
%  Eyelink('command','active_eye = LEFT');
  Eyelink('command','automatic_calibration_pacing=1500');
    % what events (columns) are recorded in EDF:
  Eyelink('command','file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
    % what samples (columns) are recorded in EDF:
  Eyelink('command','file_sample_data = LEFT,RIGHT,GAZE,HREF,AREA,GAZERES,STATUS');
    % events available for real time:
  Eyelink('command','link_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON');
    % samples available for real time:
  Eyelink('command','link_sample_data = LEFT,RIGHT,GAZE,GAZERES,AREA,STATUS');
  Eyelink('Openfile',eyelinkfile);
  fprintf('Please perform calibration. When done, the subject should press a button in order to proceed.\n');
  EyelinkDoTrackerSetup(el);
%  EyelinkDoDriftCorrection(el);
  fprintf('Button detected from subject. Starting recording of eyetracking data. Proceeding to stimulus setup.\n');
  Eyelink('StartRecording');
  % note that we expect that something should probably issue the command:
  %   Eyelink('Message','SYNCTIME');
  % before we close out the eyelink.

end

% call ptviewmovie
timeofptviewmoviecall = datestr(now);

[timeframes,timekeys,digitrecord,trialoffsets] = ptviewmovie(images, ...
frameorder,framecolor,frameduration,fixationorder,fixationcolor,fixationsize,grayval,[],[], ...
  offset,choose(con==100,[],1-con/100),movieflip,[],[],tfun,[],[], ...
  triggerkey,[],[],maskimages,specialoverlay);
         
%%%%%%%%%%%%%%%%%%%%%%%%%%% check performance

% expand the multiple-keypress cases [timekeysB]
timekeysB = {};
for p=1:size(timekeys,1)
  if iscell(timekeys{p,2})
    for pp=1:length(timekeys{p,2})
      timekeysB{end+1,1} = timekeys{p,1};
      timekeysB{end,2} = timekeys{p,2}{pp};
    end
  else
    timekeysB(end+1,:) = timekeys(p,:);
  end
end

% figure out when the user pressed a button [buttontimes]
deltatime = 1;  % holding the key down for less than this time will be counted as one button press
oldkey = ''; oldkeytime = -Inf;
buttontimes = [];
for p=1:size(timekeysB,1)

  % is this a bogus key press?
  bad = isequal(timekeysB{p,2},'absolutetimefor0') | ...
        isequal(timekeysB{p,2},'trigger') | ...
        isequal(timekeysB{p,2}(1),'5') | ...
        (isequal(timekeysB{p,2},oldkey) & timekeysB{p,1}-oldkeytime <= deltatime);
  
  % if not bogus, then record the button time
  if ~bad
    buttontimes = [buttontimes timekeysB{p,1}];
    oldkey = timekeysB{p,2};
    oldkeytime = timekeysB{p,1};
  end

end

% figure out when the dot switched colors [changetimes]
seq = abs(diff(fixationorder(2:end-2))) > 0;
changetimes = timeframes(find(seq) + 1);
numtot = length(changetimes);

% figure out the number of hits [numhits]
numhits = 0;
respondtime = 1;  % the subject has this much time to press a button
for q=1:length(changetimes)
  okok = (buttontimes > changetimes(q)) & (buttontimes <= changetimes(q) + respondtime);
  if any(okok)
    numhits = numhits + 1;
    buttontimes(firstel(find(okok))) = [];  % remove!
  end
end

% figure out the number of false alarms [numfalse]
numfalse = length(buttontimes);

% report score
fprintf('==============================================================\n');
fprintf('Out of %d events, you had %d hits and %d false alarms.\nYour score is %d/%d = %d%%.\n', ...
        numtot,numhits,numfalse,numhits-numfalse,numtot,round((numhits-numfalse)/numtot*100));
fprintf('==============================================================\n');

% prepare score string for subject
str0 = sprintf('Out of %d events, you had \n%d hits and %d false alarms.\nYour score is %d/%d = %d%%.\n', ...
               numtot,numhits,numfalse,numhits-numfalse,numtot,round((numhits-numfalse)/numtot*100));

% render as an image
%close all;
hcpgui = findall(0,'tag','hcpstim_gui');
close(setdiff(findall(0,'type','figure'),hcpgui));

drawnow; fig = figure; setfigurepos([100 100 700 700]);
drawtext(0,0,0,'Helvetica',.05,[0 0 0],[1 1 1],str0);
axis off;
im = renderfigure(700,2);
close(fig);

% show it to the subject
ptviewimage(uint8(255*im));
ptoff(oldclut);  % unsetup PT

% close out eyelink
if ~isempty(eyelinkfile)
    Eyelink('StopRecording');
    Eyelink('CloseFile');
    
    % 20140528AV
    % [status =] Eyelink('ReceiveFile',['filename'], ['dest'], ['dest_is_path'])
    status = Eyelink('ReceiveFile',eyelinkfile, pathToSaveFileIn,1);
    fprintf('ReceiveFile status %d\n', status);
    %rename it something unique
    mycmd=['mv ' pathToSaveFileIn eyelinkfile ' ' pathToSaveFileIn eyelinkfile_unique];
    system(mycmd);
    
    Eyelink('ShutDown');
end

%% Convert edf to asc
mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn eyelinkfile_unique];
system(mycmd);

if subjnum
    clear('a','im','images','mashgaponecyclefun','maskimages');
    save(fullfile(pathToSaveFileIn,filename));
end