if ~exist('kspr','var')
    run kendrickstartup
end

cd ~/Documents/MATLAB
sca

is_hcpgui = false;
if(exist('hcpobj','var'))
    d = datenum(hcpobj.expiration,hcpobj.dateformat);
    if(now <= d && strcmp(hcpobj.stimtype,'mov'))
        subjnum = hcpobj.subjnum;
        runNum = hcpobj.runnum;
        clear hcpobj;
        is_hcpgui = true;
    else
        %command expired... do not run
        clear hcpobj;
        return;
    end
end

if(~is_hcpgui)
    % 20140601AJC
    if exist('subjnum','var') %&& isempty(subjnum)
        sprintf('Is subj id: %d (y/n)? ', subjnum),
        answeryn = input('> ', 's');
        if answeryn == 'n'
            clear('subjnum');
        end
    end
    if ~exist('subjnum','var')
        disp('(subj id = 0 for no recording)');
        subjnum = input('What is the subj id? ');
    end

    disp('|-------------------------------------------')
    disp('|    runNum 0 = test + audio adjust (~3.5 min)')
    disp('|    runNum 11 = test + question1 (~23 sec)')
    disp('|    runNum 12 = test + question2 (~17 sec)')
    disp('|    runNum 1 = movie run 1')
    disp('|    runNum 2 = movie run 2')
    disp('|    runNum 3 = movie run 3')
    disp('|    runNum 4 = movie run 4')
    %disp('|    runNum 13 = fixation cross')
    disp('|--------------------------------------------')
    runNum = input('Which run number to run? > ');
end

filename = sprintf('%d_MOV%d_7T_%s', subjnum, runNum, gettimestring);

pathToSaveFileIn=sprintf('/Users/7Tuser/Desktop/HCP/Data/%d/',subjnum);
if(~exist(pathToSaveFileIn,'dir'))
    mkdir(pathToSaveFileIn);
end

movieanswer = '';

switch runNum
    case 0
        moviename = '7T_MN_test_newWav.mp4';
        %subjshow = 'Using the controller, adjust volume until voices are loud are clear: press 1 to decrease volume, press 4 to increase volume';
        subjshow = 'Please listen for the dialogue in this movie clip and let the operator know if you cannot hear what is being said';
    case 11
        moviename = 'audio_test_question1.mp4';
        subjshow = 'How long ago did he and his family come down to the Bahamas?';
        movieanswer = '10 years';
    case 12
        moviename = 'audio_test_question2.mp4';
        subjshow = 'The guide has the ability to find what?';
        movieanswer = 'A rhythm [between the tides, the clouds, what the fish are doing, and to get us in the right spot]';
    case 1
        moviename = '7T_MN_CC_1_newWav.mp4';
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 2 
        moviename = '7T_MN_CUTTING_FINAL_1_newWav.mp4';
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 3
        moviename = '7T_MN_CC_2_newWav.mp4';
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 4
        moviename = '7T_MN_CUTTING_FINAL_2_newWav.mp4';
        subjshow = 'Loading movie clips... Please try to stay awake!';
    case 13
        moviename = 'fixation';
        subjshow = 'Loading fixation cross... Please try to stay awake!';
    otherwise
        error('unknown runNum')
end

%% Schedule start immediately 
eyetracking = 0; i=0;
% Switch KbName into unified mode: It will use the names of the OS-X
% platform on all platforms in order to make this script portable:
KbName('UnifyKeyNames');
ttlPulse = KbName('5%'); % 20140120AV
space=KbName('SPACE');
esc=KbName('ESCAPE');
ttlStamps = zeros(1,16*60); % store clock stamps 20140430AV
deviceIndex = [];

try
    AssertOpenGL;
    %background=[128, 128, 128]; % original grey
    background=[255, 255, 255]; %white
    
    % Open onscreen window:
    oldLevel = Screen('Preference', 'Verbosity', 2); %quiet! default=3
    screen=max(Screen('Screens'));
    [win,screenRect] = Screen('OpenWindow', screen, 0);

    % Eyetracker prep
    switch runNum
        case {1 2 3 4}    
            el.targetbeep=0; % no beep
            %open a link to the eyetracker, passing info about the PTB screen
            el=EyelinkInitDefaults(win);
            %check the connection to the eyelink. If it's not there, exit gracefully
            if ~EyelinkInit
                fprintf('Eyelink Init aborted.\n');
                sca;
                break;
            end
            %make sure the beeps are silenced?
            % you need to pass the el structure back to the dispatch function
            if ~isempty(el.callback)
                PsychEyelinkDispatchCallback(el);
            end
            eyetracking = 1;
            % define beep sounds (frequency, volume, duration);
            el.cal_target_beep=[0 0 0];
            el.drift_correction_target_beep=[0 0 0];
            el.targetbeep = false;
            el.calibration_failed_beep = [0 0 0];
            el.calibration_success_beep = [0 0 0];
            el.drift_correction_failed_beep = [0 0 0];
            el.drift_correction_success_beep = [0 0 0];
            EyelinkUpdateDefaults(el);
            
            %open a data file on the eyetracker host
            %THE DATA FILENAME IS LIMITED TO 8.3 ON THE HOST !!!!!!!!!!!!!!
            dataFileEL=[datestr(now,'yymmdd') '.edf'];
            Eyelink('Openfile',dataFileEL);
            %optional setup information to transmit
            %users can overwrite during live calibration
            Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0, screenRect(3), screenRect(4));
            Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, screenRect(3), screenRect(4));
            Eyelink('command', 'calibration_type = HV5'); % set calibration type.
            disp('Waiting for eyetracker calibration...');
            EyelinkDoTrackerSetup(el); %run calibration          
            Eyelink('StartRecording'); %start recording
            % record a few samples before we actually start displaying
            WaitSecs(0.2);   
    end
    
    Screen('FillRect', win, background); % Clear screen to background color
    Screen('Flip',win);  % Initial display and sync to timestamp
    blocking = 1; % Use blocking wait for new frames by default
    
    ttlCnt = 0; % counting number of TTLs
        
    % Show title while movie is loading/prerolling: 
    oldtextsize = Screen('TextSize', win, 60);
    %DrawFormattedText(win, tstring, sx, sy, color, wrapat, 
    %                   flipHorizontal, flipVertical, vSpacing, righttoleft)
    DrawFormattedText(win, subjshow, 'center', 'center', [0 0 0], 30, [], [], 1.5);
    Screen('Flip', win);
    fullmoviename = [ pwd filesep moviename ];
    
    % Open movie file and retrieve basic info about movie:
    [movie movieduration fps imgw imgh] = Screen('OpenMovie', win, fullmoviename, [], []);

    fprintf('Movie: %s  : %f seconds duration, %f fps, w x h = %i x %i...\n',...
        fullmoviename, movieduration, fps, imgw, imgh);

    did_escape = false;
    % Wait for TTL before starting movie (20140104AV)
    [keyIsDown,secs,keyCode]=KbCheck(-3);
    disp('Waiting for TTL pulse...')
    while ~keyIsDown
        [keyIsDown,secs,keyCode]=KbCheck(-3);
        
        if (keyIsDown==1 && keyCode(esc))
            did_escape = true;
            break;
        end;
        
        if ~(keyIsDown && keyCode(ttlPulse))
            keyIsDown = 0;
        end
                

        
    end
    
    if(did_escape)
        return;
    end
    
    Screen('FillRect', win, [0 0 0]); %back to black
    % Play 'movie', at a playbackrate = 1, with endless loop=0 and 1.0 == 100% audio volume.
    %Screen('PlayMovie', movie, rate, loop, audio);
    Screen('PlayMovie', movie, 1, 0, 1.0);

    % 20140501AV
    KbQueueCreate(deviceIndex);
    while KbCheck; end % Wait until all keys are released.
    KbQueueStart(deviceIndex);

    disp('TTL pulse detected, starting movie...')
    timeStart = GetSecs;
    ttlCnt = ttlCnt+1;
    ttlStamps(ttlCnt) = timeStart;
    if eyetracking
          Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
    end

    % Infinite playback loop: Fetch video frames and display them...
    while 1
        
        [ keyIsDown, keyCode]=KbQueueCheck(deviceIndex); % 20140501AV  
        secs = keyCode(logical(keyCode));
        %[keyIsDown,secs,keyCode]=KbCheck(-3); %#ok<ASGLU>

        % log time stamps for ttl pulses 20140430AV
        if (keyIsDown==1 && keyCode(ttlPulse))
            ttlCnt = ttlCnt+1;
            ttlStamps(ttlCnt) = secs;
            if eyetracking
                Eyelink('Message', 'TRIALID %d', ttlCnt); % 20140526AV
            end
        end;
        
        if (keyIsDown==1 && keyCode(esc))
            break;
        end;
        
        % Only perform video image fetch/drawing if playback is active
        % and the movie actually has a video track (imgw and imgh > 0):
        if ((imgw>0) && (imgh>0))
            % Return next frame in movie, in sync
            % tex is either the positive texture handle or zero if no
            tex = Screen('GetMovieImage', win, movie, blocking);

            % Valid texture returned?
            if tex < 0 % No? Abort playback loop:
                disp('invalid texture returned)')
                break;
            end

            if tex == 0 % Just sleep a bit and then retry.
                WaitSecs('YieldSecs', 0.005);
                continue;
            end

            % Draw the new texture immediately to screen:
            Screen('DrawTexture', win, tex, [], [], [], [], [], [], []);
            Screen('Flip', win); % Update display
            Screen('Close', tex); % Release texture
            
            i=i+1; % Framecounter
        end
    end
    
    timeEnd = GetSecs;
    duration = timeEnd - timeStart;
    fprintf('Elapsed time %f seconds, for %i frames.\n', duration, i);

    Screen('Flip', win);
    KbReleaseWait;
    
    Screen('PlayMovie', movie, 0); % Done. Stop playback
    Screen('CloseMovie', movie); % Close movie object
    
    % 20140501AV
    KbEventFlush(deviceIndex);
    KbQueueRelease(deviceIndex);
        
    % Eyetracker cleanup
      if eyetracking     
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile'); %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_MOVeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        
        fprintf('Shutting down eyetracker...\n');
        Eyelink('Shutdown'); %terminate communication with eyetracker
        fprintf('\n\nConverting .edf to ASCII....\n');
        mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
      end
    Screen('CloseAll');
    
catch err%#ok<*CTCH>
    err
    % Error handling: Close all windows and movies, release all resources.
    if eyetracking        
        %after experiment ends
        Eyelink('StopRecording'); %stop recording
        Eyelink('CloseFile');  %close the datafile
        status = Eyelink('ReceiveFile',dataFileEL, pathToSaveFileIn,1); %retrieve the data file
        fprintf('ReceiveFile status %d\n', status);
        %rename it something unique
        dataFileELnew = sprintf('%d_MOVeyetrack%d_7T_%s.edf', subjnum, runNum, gettimestring); % AV 20140526                        
        mycmd=['mv ' pathToSaveFileIn dataFileEL ' ' pathToSaveFileIn dataFileELnew];
        system(mycmd);
        Eyelink('Shutdown');  %terminate communication with eyetracker
        mycmd=['/Applications/EyeLink/EDF_Access_API/Example/edf2asc ' pathToSaveFileIn dataFileELnew];
        system(mycmd);    
    end
    Screen('CloseAll');
end

if(~isempty(movieanswer))
    fprintf('\n\nQuestion: %s\nCorrect Answer: %s\n\n', subjshow, movieanswer);
end

if subjnum
    save(fullfile(pathToSaveFileIn,filename),'duration','ttlCnt','ttlStamps','timeStart','timeEnd')
end

FlushEvents
