function intraOralStim
% Code to run during intraoral stimulus runs with multiple purposes
% 1) Start when scanner pulse comes in.  - Code hasn't been implemented
% yet.  WaitTrigger.m code works though.
% 2) Tell operator when to provide stimulus - Works & tested.
% 3) Record all button presses from user - Works & tested.
% 4) Provide audible beeps to subject at 15s and 60s (or specified afterSensationDuration) after run - 'Beeper'

steadyState = 19;    % Time at beginning of scan before first stimulus.  Units are in seconds.  DEFAULT: 19
ISI = 2;       % Time between stimuli.  DEFAULT: 2,5,10 
triggerButton = 0; % 1 for key press, 0 for magnet
screenNumber = 1; % 0 for main monitor, 1 for secondary display, etc.
numTrials = 10; % This should be set to 10.  DEFAULT: 10
KbQueueCreate; % Create queue for later recording of button-box input

global afterSensationDuration;
afterSensationDuration = 10;    % When to provide FINAL audible beep to subject (first beep happens at 15s anyway). DEFAULT 60
dir_status=mkdir('~/Matlab_scripts/intraOralStim_output');
outputDir = '~/Matlab_scripts/intraOralStim_output'
filename = input('Filename to save data:  ','s');
timeStamp = datestr(now,'HHMMSS');
filename = strcat(filename,'_',timeStamp);

global responseList;
global stimulusList;
global scanStartTime;

try
    % Set up window
    w=Screen('OpenWindow', screenNumber);
    Screen('FillRect', w);
    % Find center of screen
    [winwidth winheight] = Screen('WindowSize', w);
    xCenter = winwidth/2;
    yCenter = winheight/2;

    % Format text
    Screen('TextFont',w, 'Times');
    Screen('TextSize',w, 80 );
    
    
    %%% Trigger to start
    if triggerButton == 1
        Screen('DrawText',w,'Waiting for keypress (any button)...',xCenter-600, yCenter, [255, 0, 0, 255]);
        Screen('Flip',w);
        %disp('Waiting for keypress (any button)...');
        WaitSecs(0.5);
        KbWait;
        scanStartTime = GetSecs;
    else
        Screen('DrawText',w,'Waiting for trigger...',xCenter-200, yCenter, [255, 0, 0, 255]);
        Screen('Flip',w);
        getMRtrigger;
        scanStartTime = GetSecs;
        KbQueueCreate;
    end


    % Once scanner starts, we want to start listening for button presses and
    % tell the operator when to stimulate
    % Establish the time scan starts
    responseList = [0 0];   %stores subject button-box responses and time of press since scan start
    stimulusList = [0]; %stores times of stimulus onset
    Screen('TextSize',w,200);
    KbQueueStart;
    returnState = stimLoop(steadyState, w, xCenter, yCenter );
    
    % if stimLoop ends prematurely, this 'if' should allow for a smooth exit
    % from the script.  it also writes out the response & stimulus timing
    % out to a file
    if returnState == 1
        % Save file w/ timing of fixation changes and responses
        responseList = responseList(2:end,:);
        eval(['save ' eval('fullfile(outputDir,filename)') ' responseList stimulusList'])
        Screen('CloseAll');
        return;
    end

    for j=2:numTrials
        returnState = stimLoop(ISI, w, xCenter, yCenter );
        
        % if stimLoop ends prematurely, this 'if' should allow for a smooth exit
        % from the script.  it also writes out the response & stimulus timing
        % out to a file
        if returnState == 1
            % Save file w/ timing of fixation changes and responses
            responseList = responseList(2:end,:);
            eval(['save ' eval('fullfile(outputDir,filename)') ' responseList stimulusList'])
            Screen('CloseAll');
            return;
        end
    end    
    aftersensationBeep(w, xCenter, yCenter);
    KbQueueRelease;
    Screen('CloseAll');
    
    WaitSecs(0.5);
    disp('Run finished.  Here are the responses entered:');
    disp(responseList);
    disp(' ');
    disp('Here are the times the stimulus was presented: ');
    disp(stimulusList);

    % Save file w/ responses and response times and stimulus times
    responseList = responseList(2:end,:);
    eval(['save ' eval('fullfile(outputDir,filename)') ' responseList stimulusList'])
    disp('Data has been saved to a file');

catch
    % This "catch" section executes in case of an error in the "try" section
    % above.  Importantly, it closes the onscreen window if it's open.
    Screen('CloseAll');
    psychrethrow(psychlasterror);

end

 
%% stimLoop displays stimulus time to operator and signals when to provide
%% stimulus.  Also handles and records all button box input
function [returnState] = stimLoop(interStimInterval, w, xCenter, yCenter)
global responseList;
global stimulusList;
global scanStartTime;
%interStimInterval: Add 1 to variable due to an error in the code logic that loops... easier to add 1
%than to track down the bug
interStimInterval = interStimInterval+1; 
escapeKey = KbName('ESCAPE');
startTime = GetSecs;
timeRemainingOld=0;  % Used as part of a check to prevent writing out to the screen multiple times
returnState = 0;
currentlyStimulating = 0; %Default set to 0 to signify no stimulation 
while (GetSecs - startTime) < interStimInterval
    timeRemainingNew = floor(interStimInterval - (GetSecs - startTime));
    % If 1 second has passed and it's not yet time to stimulate
    if timeRemainingNew ~= timeRemainingOld && timeRemainingNew >= 1
        timeRemainingOld = timeRemainingNew;
        Screen('DrawText',w,int2str(timeRemainingNew),xCenter-100 , yCenter-100, [255, 0, 0, 255]);
        Screen('Flip',w);
        %disp(timeRemainingNew);
    elseif timeRemainingNew < 1 && currentlyStimulating == 0
        Screen('DrawText',w,'Stimulate',xCenter-300 , yCenter-100, [255, 0, 0, 255]);
        Screen('Flip',w);
        stimTime = GetSecs - scanStartTime;
        %fprintf('Stimulate: GetSecs - scanStartTime= %f\n',stimTime);
        stimulusList(end+1,:) = stimTime;
        currentlyStimulating = 1;
    end

    % Record button presses
    [ pressed, firstPress]=KbQueueCheck;
    timeSecs = firstPress(find(firstPress));
    if pressed
        % fprintf may give an error if multiple keys have been pressed
        %%fprintf('"%s" typed at time %.3f seconds\n', KbName(firstPress), timeSecs - scanStartTime);
        buttonPressed = KbName(firstPress)
        responseList(end+1,:) = [str2num(buttonPressed(1)), timeSecs - scanStartTime];

        if firstPress(escapeKey)
            returnState = 1;
            return;
        end
    end
end
return
%% Code to wait until MRtrigger occurs
function getMRtrigger
% Wait for the "5" key with KbQueueWait.  Code copied directly from
% KbQueueDemo.m
keysOfInterest=zeros(1,256);
keysOfInterest(KbName('5%'))=1; %Only respond to button 5
KbQueueCreate(-1, keysOfInterest);
%fprintf('\nWaiting for trigger... Expecting "5" input.\n');
KbQueueStart;
timeSecs = KbQueueWait;
%fprintf('Scan started!');
KbQueueRelease;
return

%% Beep 15s and 60s after run finishes.  
function aftersensationBeep(w, xCenter, yCenter)
global responseList;
global stimulusList;
global scanStartTime;
global afterSensationDuration
tmpStartTime = GetSecs;
%afterSensationDuration = 20;
returnState = 0;
escapeKey = KbName('ESCAPE');
while (GetSecs - tmpStartTime) < (afterSensationDuration + 5)
    timeRemaining = ceil(afterSensationDuration - (GetSecs - tmpStartTime));
    Screen('DrawText',w,'Aftersensations', 100,100, [255,0,0,255]);
    Screen('DrawText',w,int2str(timeRemaining),xCenter-100 , yCenter-100, [255, 0, 0, 255]);
    Screen('Flip',w);

    % Beep at 15s & at variable afterSensationDuration
    if (ceil(GetSecs - tmpStartTime) == 15) || (ceil(GetSecs - tmpStartTime) == afterSensationDuration)
        Beeper;
        stimTime = GetSecs - scanStartTime;
        stimulusList(end+1,:) = stimTime;
        WaitSecs(1);
    end
    % Record button presses
    [ pressed, firstPress]=KbQueueCheck;
    timeSecs = firstPress(find(firstPress));
    if pressed
        % Again, %fprintf will give an error if multiple keys have been pressed
        %fprintf('"%s" typed at time %.3f seconds\n', KbName(firstPress), timeSecs - scanStartTime);
        buttonPressed = KbName(firstPress)
        responseList(end+1,:) = [str2num(buttonPressed(1)), timeSecs - scanStartTime];

        if firstPress(escapeKey)
            returnState = 1;
            return;
        end
    end
end
return