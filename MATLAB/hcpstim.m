function hcpstim(varargin)

clear functions;

if(nargin < 1)
    

    close(findall(0,'type','figure'));
    hfig = hgload('hcpstim_gui.fig');
    
    scsz = get(0,'ScreenSize');
    winsz = get(hfig,'Position');
    winsz(1:2) = scsz(3:4)/2 - winsz(3:4)/2;
    set(hfig,'Position',winsz);
    
    if(evalin('base','exist(''subjnum'',''var'')'))
        subjnum = evalin('base','subjnum');
        txtsubj = findobj(hfig,'tag','txtSubj');
        if(isnumeric(subjnum))
            subjnum=num2str(subjnum);
        end
        set(txtsubj,'string',subjnum);
    end
    
else
    hbutton = varargin{2};
    whichbutton = get(hbutton,'tag');
    
    hfig = gcbf;
    txtsubj = findobj(hfig,'tag','txtSubj');
    subjstr = strtrim(get(txtsubj,'string'));
    subjnum = str2num(subjstr);

    if(isempty(subjnum))
        warning('Invalid subject number: %s',subjstr);
        return;
    end
    
    global did_forcequit;
    did_forcequit = false;
    expire_sec = 1;

    %to do: don't continue if error or ctrl+c
    
    switch(whichbutton)
        case 'run_reset'
            evalin('base','clear functions; clear mex; hcp_clear; clear all;');
            
        case 'run_fix1'
            run_hcpcommand('fix',subjnum,1,expire_sec);
            
        case 'run_fix2'
            run_hcpcommand('fix',subjnum,2,expire_sec);
            
        case 'run_fix3'
            run_hcpcommand('fix',subjnum,3,expire_sec);
            
        case 'run_fix4'
            run_hcpcommand('fix',subjnum,4,expire_sec);
            
        case 'run_sound1'
            run_hcpcommand('mov',subjnum,11,expire_sec);
            
        case 'run_sound2'
            run_hcpcommand('mov',subjnum,12,expire_sec);
            
        case 'run_mov1'
            run_hcpcommand('mov',subjnum,1,expire_sec);
            
        case 'run_mov2'
            run_hcpcommand('mov',subjnum,2,expire_sec);
            
        case 'run_mov3'
            run_hcpcommand('mov',subjnum,3,expire_sec);
            
        case 'run_mov4'
            run_hcpcommand('mov',subjnum,4,expire_sec);
            
        case 'run_ret1'
            run_hcpcommand('ret',subjnum,1,expire_sec);
            
        case 'run_ret2'
            run_hcpcommand('ret',subjnum,2,expire_sec);
            
        case 'run_ret3'
            run_hcpcommand('ret',subjnum,3,expire_sec);
            
        case 'run_ret4'
            run_hcpcommand('ret',subjnum,4,expire_sec);
            
        case 'run_ret5'
            run_hcpcommand('ret',subjnum,5,expire_sec);
            
        case 'run_ret6'
            run_hcpcommand('ret',subjnum,6,expire_sec);

        case 'runall_ret'
            try
                run_hcpcommand('ret',subjnum,1,expire_sec);
                run_hcpcommand('ret',subjnum,2,expire_sec);
                run_hcpcommand('ret',subjnum,3,expire_sec);
                run_hcpcommand('ret',subjnum,4,expire_sec);
                run_hcpcommand('ret',subjnum,5,expire_sec);
                run_hcpcommand('ret',subjnum,6,expire_sec);
            catch err
                fprintf('\n\n');
                warning(err.message);
            end
            
        case 'findsubject'
            newsubj = subject_selector();
            if(~isempty(newsubj))
                set(txtsubj,'string',num2str(newsubj));
            end
            
        case 'checkfiles'
            [iscomplete, stimtype_summary] = check_subject_files(subjnum);
            
        otherwise
    end
    
end
